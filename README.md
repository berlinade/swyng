# swyng

## (A Short) Description

this package provides numerical solvers mostly for daes 
(differential algebraic equations)

## Getting Started

### Installation

... via pip from PYPI (python package index)
```bash
pip install swyng
```

... or locally (using pip)
```bash
git clone https://gitlab.com/berlinade/swyng.git
cd swyng
pip install -e .
```

### Usage

... *under construction*

## Documentation

... can be found here (*under construction*): [https://berlinade.gitlab.io/swyng/](https://berlinade.gitlab.io/swyng/)

## Find swyng 

- @ GitLab: [https://gitlab.com/berlinade/swyng](https://gitlab.com/berlinade/swyng)
- @ PYPI: [https://pypi.org/project/swyng/](https://pypi.org/project/swyng/)
- auther: [codima on youtube.com](https://www.youtube.com/channel/UCwnthITQqkWgaHnz82U7WsA)

## (Informal Info on) License

this version of the "swyng" has been published under GPLv3 (see [LICENSE](https://gitlab.com/berlinade/polynom/-/blob/main/LICENSE)). <br> 
Alternative Licenses are negotiable.
