import numpy as np

from typing import Callable


sin = np.sin


def sys_func(d_x: np.ndarray, x: np.ndarray, t: float) -> np.ndarray:
    g, length = 9.81, 0.8  # [m/s**2], [m]
    d_phi, d_omega = d_x
    phi, omega = x
    return np.array([omega - d_phi, (g/length)*sin(phi) + d_omega])


def event_func(x: np.ndarray, t: float) -> np.ndarray:
    phi, omega = x
    if t > 5.0: return np.array([phi, phi])
    return np.array([phi, -1.0])
