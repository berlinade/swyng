import numpy as np

from system_function import sys_func, event_func
from swyng.dae.euler import integrate, EventOpts

from swyng.nl.scipy_wrapper import RootWrapper
from swyng.nl.newton import Newton, NewtonOpts
from swyng.nl.homotopy import Homotopy

import matplotlib
import matplotlib.pyplot as plt

from typing import Callable, Optional


sin = np.sin


def plot_sol(ts: list[float], xs: list[np.ndarray], events: list[int], title: str) -> None:
    fig, ax = plt.subplots()
    ax.plot(ts, xs)
    ax.plot([ts[e_i] for e_i in events], [xs[e_i][0] for e_i in events], 'ro')
    ax.plot([ts[e_i] for e_i in events], [xs[e_i][1] for e_i in events], 'bo')

    ax.set(xlabel = 'time (s)', ylabel = 'angle/angle_speed',
           title = title)
    ax.grid()


def main():
    t0: float = 0.0  # [s], initial time
    T: float = 15.0  # [s], terminal time
    hh: float = 0.05  # [s], general step-width
    x0: np.ndarray = np.array([np.pi/2.0, 0.0])  # phi_0, omega_0

    event_ops = EventOpts(orientation = [-1.0, 1.0],
                          terminating = [False, True])

    # nl_root = RootWrapper()
    newton = Newton(logger = False, opts = NewtonOpts(refresh_jac = 4))
    # homotopy = Homotopy(nl_solver = newton)
    out = integrate(sys_func, x0, t0, T,
                    event_func = event_func,
                    opts = dict(h = hh, err_tol = 1.0e-3),
                    event_opts = event_ops,
                    nl_solver = newton)
    print(out.event_ids)
    plot_sol(out.ts, out.xs, out.events, 'math pendulum - computed "classically"')


if __name__ == '__main__':
    main()

    plt.show()
