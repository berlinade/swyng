
  nl\_root
-----
**declaration**

```python
def nl_root(func: Callable[[np.ndarray], np.ndarray], x0: np.ndarray, jac: Callable[[np.ndarray], np.ndarray], atol: float = 1e-08, rtol: float = 1e-05, logger: logging.Logger | bool | str = True, opts: Optional[dict[str, Any]] = None) -> Report: 
```



-----