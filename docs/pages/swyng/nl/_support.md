
▸ Report
-----
**declaration**

```python
class Report(object): 
```



??? abstract "member functions"

    ▹ Report.\_\_init\_\_

    ▹ Report.\_\_getitem\_\_

    ▹ Report.\_\_setitem\_\_

-----
▹ Report.\_\_init\_\_
-----
**declaration**

```python
def __init__(self, x: Optional[np.ndarray] = None, success: Optional[bool] = None): 
```



-----
▹ Report.\_\_getitem\_\_
-----
**declaration**

```python
def __getitem__(self, item: str): return self.__getattribute__(item) 
```



-----
▹ Report.\_\_setitem\_\_
-----
**declaration**

```python
def __setitem__(self, name: str, val: Any): return self.__setattr__(name, val) 
```



-----
▸ NLSolver
-----
**declaration**

```python
class NLSolver(Protocol): 
```



??? abstract "member functions"

    ▹ NLSolver.\_\_call\_\_

-----
▹ NLSolver.\_\_call\_\_
-----
**declaration**

```python
def __call__(self, func: Callable[[np.ndarray], np.ndarray], x0: np.ndarray, jac: Callable[[np.ndarray], np.ndarray] | None = None, atol: float = 1e-08, rtol: float = 1e-05, logger: logging.Logger | bool | str = True, opts: Optional[dict[str, Any]] = None) -> Report: ... 
```



-----