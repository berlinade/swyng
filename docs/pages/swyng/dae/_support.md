
▸ SysFunc
-----
**declaration**

```python
class SysFunc(Protocol): 
```



??? abstract "member functions"

    ▹ SysFunc.\_\_call\_\_

-----
▹ SysFunc.\_\_call\_\_
-----
**declaration**

```python
def __call__(self, y: np.ndarray, x: np.ndarray, s: float) -> np.ndarray: ... 
```



-----
▸ LeadingDerivativeTerm
-----
**declaration**

```python
class LeadingDerivativeTerm(Protocol): 
```



??? abstract "member functions"

    ▹ LeadingDerivativeTerm.\_\_call\_\_

-----
▹ LeadingDerivativeTerm.\_\_call\_\_
-----
**declaration**

```python
def __call__(self, x: np.ndarray, s: float) -> np.ndarray: ... 
```



-----
▸ SysJac
-----
**declaration**

```python
class SysJac(Protocol): 
```



??? abstract "member functions"

    ▹ SysJac.\_\_call\_\_

-----
▹ SysJac.\_\_call\_\_
-----
**declaration**

```python
def __call__(self, alpha: float, y: np.ndarray, x: np.ndarray, s: float) -> np.ndarray: ... 
```



-----
▸ EventFunc
-----
**declaration**

```python
class EventFunc(Protocol): 
```



??? abstract "member functions"

    ▹ EventFunc.\_\_call\_\_

-----
▹ EventFunc.\_\_call\_\_
-----
**declaration**

```python
def __call__(self, x: np.ndarray, s: float) -> np.ndarray: ... 
```



-----