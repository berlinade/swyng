
▸ EulerOpts
-----
**declaration**

```python
class EulerOpts(OptionsTemplate): 
```



??? abstract "member functions"

    ▹ EulerOpts.\_\_init\_\_

-----
▹ EulerOpts.\_\_init\_\_
-----
**declaration**

```python
def __init__(self, h: float | None = None, h_max: float | None = None, h_min: float | None = None, h_damp: float = 0.9, checkpoints: tuple[float, ...] | None = None, atol: float = 1e-08, rtol: float = 1e-05, err_tol: float = 1e-05): 
```



-----
▸ EulerEventOpts
-----
**declaration**

```python
class EulerEventOpts(OptionsTemplate): 
```



??? abstract "member functions"

    ▹ EulerEventOpts.\_\_init\_\_

-----
▹ EulerEventOpts.\_\_init\_\_
-----
**declaration**

```python
def __init__(self, terminating: np.ndarray | Sequence[float] = (0.0,), orientation: np.ndarray | Sequence[bool] = (False,), event_tol: float = 1e-05): 
```



-----
▸ EulerResult
-----
**declaration**

```python
class EulerResult(OptionsTemplate): 
```



??? abstract "member functions"

    ▹ EulerResult.\_\_init\_\_

-----
▹ EulerResult.\_\_init\_\_
-----
**declaration**

```python
def __init__(self, ts: list[float], hs: list[float], xs: list[np.ndarray], events: list[int], event_ids: list[list[int]], opts: EulerOpts, event_opts: EulerEventOpts): 
```



-----
  integrate
-----
**declaration**

```python
def integrate(sys_func: SysFunc, x0: np.ndarray, t0: float, t_end: float, sys_jac: SysJac | None = None, derivative_term_func: LeadingDerivativeTerm | None = None, event_func: EventFunc | None = None, nl_solver: NLSolver | None = None, logger: logging.Logger | bool | str = True, opts: EulerOpts | dict[str, Any] | None = None, event_opts: EulerEventOpts | None = None): 
```


x' = F(x)  # ODE
x_n = x_a + h*F(x_a)  # forward Euler
0 = (x_n - x_a) - h*F(x_n)  # backward/implicit Euler


0 = f( (d/dt) d(x(t), t) , x(t), t)  # DAE

f(d, x, s)

DIFF = (d(x_n, t_n) - d(x_a, t_a))/h  approx  (d/dt) d(x(t), t)
0 = H(x_n) = f( DIFF, x_n, t_n )  # implicit Euler

Newton Vorschrift: J_H[x_n]^{-1} * (x_nn - x_n) = -H(x_n)

J_H := (p_y) f( DIFF, x_n, t_n )*[(p_x) d(x_n, t_n)]/h + (p_x) f( DIFF, x_n, t_n )

def J_tilde(alpha, w, v, t_n) = alpha * (p_y) f(y, x, s)*[(p_x) d(x, s)] + (p_x) f(y, x, s)



-----
  create\_h\_func
-----
**declaration**

```python
def create_h_func(x_o: np.ndarray, t_o: float, t_n: float, h_n: float, sys_func, d_func): 
```


Generates the $H(x)$ function representing an implicit Euler step, where

$$
H({\bf x}) \equiv f\left( \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n}, {\bf x}, t_{n+1} \right)
$$

**args**

  - x_o: or $x_n$ - the 'old'  aka  preceding  aka  the latest known approximation to $x(t)$
  - t_o: or $t_n$ - the 'old' time point corresponding to x_o
  - t_n: or $t_{n+1} \equiv t_n + h_n$ - the 'new' time point corresponding to (the yet unknown) x_n
  - h_n: or $h_n \equiv t_{n+1} - t_n$ the current time step width
  - sys_func: the dae outer system function
  - d_func: the (dae inner) leading derivative term expression



-----
  create\_h\_jac\_from\_sys\_jac
-----
**declaration**

```python
def create_h_jac_from_sys_jac(x_o: np.ndarray, t_o: float, t_n: float, h_n: float, sys_jac, d_func): 
```


Generates the function computing the Jacobian $J_H[{\mathbf x}]$ of $H(x)$, where

$$
H({\bf x}) \equiv f\left( \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n}, {\bf x}, t_{n+1} \right)
$$

via the user supplied "sys_jac" function

$$
\mathrm{sys\_jac}(\alpha, y, x, s) \equiv \alpha\cdot
  \left[\partial_y f(y, x, s)\right] \cdot \left[\partial_x d(x, s)\right] +
  \left[\partial_x f(y, x, s)\right]
$$

as follows

$$
\mathrm{sys\_jac}\left(\tfrac 1h, \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n},
  {\mathbf x}, t_{n+1}\right) = J_H[{\mathbf x}]
$$

**args**

  - x_o: or $x_n$ - the 'old'  aka  preceding  aka  the latest known approximation to $x(t)$
  - t_o: or $t_n$ - the 'old' time point corresponding to x_o
  - t_n: or $t_{n+1} \equiv t_n + h_n$ - the 'new' time point corresponding to (the yet unknown) x_n
  - h_n: or $h_n \equiv t_{n+1} - t_n$ the current time step width
  - sys_jac: the optionally user-provided system Jacobian
  - d_func: the (dae inner) leading derivative term expression

**returns**

  - the function computing the Jacobian



-----
  create\_h\_approx\_jac
-----
**declaration**

```python
def create_h_approx_jac(h_func, dx: float = 1e-08): 
```


generates a function that uses finite differences to approximate the Jacobian $J_H[{\mathbf x}]$ of $H(x)$, where

$$
H({\bf x}) \equiv f\left( \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n}, {\bf x}, t_{n+1} \right)
$$

**args**

  - h_func: system function at latest time-step $t_{n+1}$
  - dx: resolution of finite difference approximation

**returns**

  - the function approximating $J_H[{\mathbf x}]$



-----