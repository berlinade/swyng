import numpy as np

from typing import Union, Any, Protocol, Optional


class VectorFunction(Protocol):
    def __call__(self, x: np.ndarray) -> np.ndarray: ...


def finite_directional_diff(func: VectorFunction,
                            x: np.ndarray,
                            dx: np.ndarray,
                            fx: Optional[np.ndarray] = None,
                            prec: Optional[float] = 1.0e-8,
                            norm_by_dx: bool = True) -> tuple[np.ndarray, np.ndarray]:
    ''' TODO: smarte doc '''
    if fx is None: fx = func(x)

    if norm_by_dx: denominator: float = prec*np.linalg.norm(dx)
    else: denominator: float = prec

    return (func(x + prec*dx) - fx)/denominator, fx


def canonical_unit_vector(dim: int, i: int) -> np.ndarray:
    ''' TODO: smarte doc '''
    return np.eye(1, dim, i).reshape((-1,))


def generate_jac_fd(func: VectorFunction, prec: float = 1.0e-8):
    ''' TODO: smarte doc '''

    def jac_fd(x_n: np.ndarray) -> np.ndarray:  # <tomarkdown:IGNORE>
        dim = x_n.shape[0]
        jac = np.empty(shape = (dim, dim))
        fx_n: np.ndarray = func(x_n)
        for i in range(dim): jac[:, i], fx = finite_directional_diff(func = func,
                                                                     x = x_n,
                                                                     dx = canonical_unit_vector(dim, i),
                                                                     fx = fx_n,
                                                                     norm_by_dx = False)  # because normed already
        return jac
    return jac_fd
