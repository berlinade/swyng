import numpy as np

from swyng.support import OptionsTemplate

from typing import Union, Any, Protocol

from collections.abc import Sequence


class SysFunc(Protocol):
    def __call__(self, y: np.ndarray, x: np.ndarray, s: float) -> np.ndarray: ...


class LeadingDerivativeTerm(Protocol):
    def __call__(self, x: np.ndarray, s: float) -> np.ndarray: ...


def derivative_term_func_default(x: np.ndarray, s: float): return x


class SysJac(Protocol):
    def __call__(self, alpha: float, y: np.ndarray, x: np.ndarray, s: float) -> np.ndarray: ...


class EventFunc(Protocol):
    def __call__(self, x: np.ndarray, s: float) -> np.ndarray: ...


class EventOpts(OptionsTemplate):
    terminating: np.ndarray | Sequence[float] = (0.0,)
    orientation: np.ndarray | Sequence[float | int] = (False,)

    def __init__(self,
                 terminating: np.ndarray | Sequence[float] = (0.0,),
                 orientation: np.ndarray | Sequence[float | int] = (1,),
                 event_tol: float = 1.0e-5):
        self.terminating = np.array(terminating)
        self.orientation = np.array(orientation)


class Stepper(object):  # <tomarkdown:IGNORE>
    eps: float = None

    t_start: float = None
    t_end: float = None
    ts: list[float] = None
    t_checkpoints: list[float] = None

    h_min: float = None
    h_max: float = None
    hs: list[float] = None

    h_next: float = None
    t_next: float = None

    finished: bool = None

    def __init__(self,
                 t_start: float,
                 t_end: float,
                 step_width_start: float | None = None,
                 step_width_min: float | None = None,
                 step_width_max: float | None = None,
                 checkpoints: tuple[float, ...] | None = None):
        self.eps: float = 1.0e-14

        self.t_start = t_start
        self.t_end = t_end
        self.ts = [self.t_start]

        t_diameter: float = self.t_end - self.t_start
        if step_width_start is None: step_width_start = t_diameter*1.0e-3
        if step_width_min is None: step_width_min = t_diameter*1.0e-9
        if step_width_max is None: step_width_max = t_diameter*1.0e-1
        self.h_min = step_width_min
        self.h_max = step_width_max
        self.hs = list()

        self.h_next = step_width_start

        if (checkpoints is None) or (checkpoints is False): checkpoints = tuple()
        self.t_checkpoints = sorted(checkpoints, reverse = True)

        self.finished = False

    def _prepare_next(self):
        ''' smarte doc '''

        ''' sort out old checkpoints '''
        while (len(self.t_checkpoints) > 0) and (self.t_checkpoints[-1] < self.ts[-1] + self.eps):
            self.t_checkpoints.pop()

        ''' determine next irregular stop '''
        t_next_stop: float = self.t_end
        if len(self.t_checkpoints) > 0: t_next_stop = min(t_next_stop, self.t_checkpoints[-1])

        ''' compare current step against next irregular step '''
        t_cand: float = self.ts[-1] + self.h_next  # cand - candidate
        if t_cand > t_next_stop - self.eps:
            self.h_next = t_next_stop - self.ts[-1]
            t_cand = t_next_stop

        ''' store next point in time '''
        self.t_next = t_cand

    def __iter__(self): return self

    def __next__(self):
        if self.finished: raise StopIteration
        self._prepare_next()
        return self.t_next, self.h_next

    def accept(self):
        self.finished = (self.t_next >= self.t_end - self.h_min + self.eps)

        self.ts.append(self.t_next)
        self.hs.append(self.h_next)

    def reject(self, h_next: float):
        if (h_next < self.h_next) and (self.h_next <= self.h_min + self.eps): raise AssertionError('h_n below h_min!')
        self.h_next = max(self.h_min, min(self.h_max, h_next))
