import numpy as np

from swyng.support import prep_logger, logging

from swyng.deriv.finiteDiff import generate_jac_fd

from swyng.nl.support import NLSolver, NLReport
from swyng.nl.newton import Newton

from swyng.dae.support import (SysFunc, LeadingDerivativeTerm, derivative_term_func_default, SysJac, EventFunc,
                               OptionsTemplate, EventOpts, Stepper)

from typing import Optional, Any


class EulerOpts(OptionsTemplate):
    h: float | None = None
    h_max: float | None = None
    h_min: float | None = None
    h_damp: float = 0.9
    checkpoints: tuple[float, ...] | None = None
    atol: float = 1.0e-8
    rtol: float = 1.0e-3
    err_tol: float = 1.0e-5
    step_control: bool = True

    def __init__(self,
                 h: float | None = None, h_max: float | None = None, h_min: float | None = None, h_damp: float = 0.9,
                 checkpoints: tuple[float, ...] | None = None,
                 atol: float = 1.0e-8, rtol: float = 1.0e-5, err_tol: float = 1.0e-5,
                 step_control: bool = True):
        self.h, self.h_max, self.h_min, self.h_damp = h, h_max, h_min, h_damp
        self.checkpoints = checkpoints
        self.atol, self.rtol, self.err_tol = atol, rtol, err_tol
        self.step_control = step_control


class EulerResult(OptionsTemplate):
    ts: list[float] = None
    hs: list[float] = None
    xs: list[np.ndarray] = None
    events: list[int] = None
    event_ids: list[list[int]] = None
    opts: EulerOpts = None
    event_opts: EventOpts = None

    def __init__(self,
                 ts: list[float], hs: list[float], xs: list[np.ndarray],
                 events: list[int], event_ids: list[list[int]],
                 opts: EulerOpts, event_opts: EventOpts):
        self.ts, self.hs, self.xs = ts, hs, xs
        self.events, self.event_ids = events, event_ids
        self.opts, self.event_opts = opts, event_opts


def integrate(sys_func: SysFunc, x0: np.ndarray,
              t0: float, t_end: float,
              sys_jac: SysJac | None = None,
              derivative_term_func: LeadingDerivativeTerm | None = None,
              event_func: EventFunc | None = None,
              nl_solver: NLSolver | None = None,
              logger: logging.Logger | bool | str = True,
              opts: EulerOpts | dict[str, Any] | None = None,
              event_opts: EventOpts | None = None):
    """
    x' = F(x)  # ODE
    x_n = x_a + h*F(x_a)  # forward Euler
    0 = (x_n - x_a) - h*F(x_n)  # backward/implicit Euler


    0 = f( (d/dt) d(x(t), t) , x(t), t)  # DAE

    f(d, x, s)

    DIFF = (d(x_n, t_n) - d(x_a, t_a))/h  approx  (d/dt) d(x(t), t)
    0 = H(x_n) = f( DIFF, x_n, t_n )  # implicit Euler

    Newton Vorschrift: J_H[x_n]^{-1} * (x_nn - x_n) = -H(x_n)

    J_H := (p_y) f( DIFF, x_n, t_n )*[(p_x) d(x_n, t_n)]/h + (p_x) f( DIFF, x_n, t_n )

    def J_tilde(alpha, w, v, t_n) = alpha * (p_y) f(y, x, s)*[(p_x) d(x, s)] + (p_x) f(y, x, s)
    """

    ''' parsing '''
    if derivative_term_func is None: d_func = derivative_term_func_default
    else: d_func = derivative_term_func

    logger = prep_logger('swyng.dae.euler', logger)

    if opts is None: opts = EulerOpts()
    elif isinstance(opts, dict): opts = EulerOpts(**opts)

    if event_opts is None: event_opts = EventOpts()
    elif isinstance(event_opts, dict): event_opts = EventOpts(**event_opts)

    ''' parsing stage 2 '''
    stepper_inst = Stepper(t_start = t0, t_end = t_end,
                           step_width_start = opts.h,
                           step_width_min = opts.h_min,
                           step_width_max = opts.h_max,
                           checkpoints = opts.checkpoints)

    atol: float = opts.atol
    rtol: float = opts.rtol
    err_tol: float = opts.err_tol
    h_damp = opts.h_damp

    step_control = opts.step_control

    terminating: np.ndarray = event_opts.terminating
    orientation: np.ndarray = event_opts.orientation

    if nl_solver is None: nl_solver: NLSolver = Newton(logger = False,
                                                       opts = dict(atol = atol, rtol = rtol))

    euler_step = generate_euler_step(nl_solver = nl_solver,
                                     sys_func = sys_func, d_func = d_func, sys_jac = sys_jac,
                                     rtol = rtol, atol = atol, logger = logger)

    ''' create data-structures for the storage of future return-output '''
    xs: list[np.ndarray] = [x0]
    ts: list[float] = stepper_inst.ts
    hs: list[float] = stepper_inst.hs
    events: list[int] = list()
    event_ids: list[list[int]] = list()

    progression_stone: float = -0.001  # for progression log message
    event_func_eval_o: np.ndarray = np.ndarray([])  # init empty for type annotation
    event_func_eval_n: np.ndarray = event_func_eval_o  # init empty for type annotation
    event_ids_o: list[int] = list()
    event_ids_event: list[int] = event_ids_o  # init empty for type annotation
    if not (event_func is None): event_func_eval_o: np.ndarray = event_func(x0, t0)
    h_mem_for_event: float | None = None
    for loop_idx, (t_n, h_n) in enumerate(stepper_inst):
        t_o = ts[-1]
        x_o = xs[-1]

        ''' full step '''
        report = euler_step(x_o, t_o, t_n, h_n)
        x_full = report.x

        if step_control and (h_mem_for_event is None):
            ''' 2 half steps '''
            h_half = h_n/2.0
            t_half = t_o + h_half
            report_a = euler_step(x_o, t_o, t_half, h_half)
            x_half = report_a.x
            report_b = euler_step(x_half, t_half, t_n, h_half)
            x_n = report_b.x
        else:
            report_a = None
            report_b = None
            x_n = x_full

        ''' processing progression log message '''
        progression = 100.0*(t_n - t0)/(t_end - t0)
        if progression >= progression_stone:
            progression_stone += 5.0
            msg1: str = f'at h: {h_n}'
            msg2: str = f'in between [t_o, t_n] = [{t_o}, {t_n}]'
            logger.info(f'[idx: {loop_idx}] progress: {progression:.2f}% {msg1} {msg2}')

        ''' catch non-convergent nl-solver '''
        shrink_h: bool = False
        if not report.success: shrink_h = True
        if step_control and (h_mem_for_event is None):
            if not report_a.success: shrink_h = True
            if not report_b.success: shrink_h = True
        if shrink_h:
            stepper_inst.reject(h_next = 0.49*h_n)
            msg1: str = f'decrease h from {h_n} to {stepper_inst.h_next}'
            msg2: str = f'in between [t_o, t_n] = [{t_o}, {t_n}]'
            logger.warning(f'[idx: {loop_idx}] NLSolver failed: --> {msg1} {msg2}')
            continue

        ''' event location '''
        if not (event_func is None):
            event_func_eval_n = event_func(x_n, t_n)
            if h_mem_for_event is None:
                out = _detect_events(t_o, x_o, t_o, x_o, t_n, x_n, euler_step,
                                     orientation, event_func,
                                     event_func_eval_a = event_func_eval_o, event_func_eval_b = event_func_eval_n)
                does_have, x_event, h_event, event_func_eval_event, event_ids_event = out
                if does_have:
                    if not (h_event is None):
                        if (h_event < h_n - stepper_inst.h_min) and (h_event > stepper_inst.h_min):
                            h_mem_for_event = h_n
                            stepper_inst.h_next = h_event
                            msg1: str = f'decrease h from {h_n} to {stepper_inst.h_next}'
                            msg2: str = f'in between [t_o, t_n] = [{t_o}, {t_n}]'
                            logger.info(f'[idx: {loop_idx}] |~~~~> [near event] {msg1} {msg2}')
                            event_ids_o = event_ids_event
                            event_func_eval_o = event_func_eval_event
                            continue
            else:  # so event has occurred and finished iteration
                stepper_inst.accept()
                events.append(len(xs))
                event_ids.append(event_ids_o)
                xs.append(x_n)
                stepper_inst.h_next = h_mem_for_event
                h_mem_for_event = None
                msg1: str = f'reset h from {h_n} to {stepper_inst.h_next}'
                msg2: str = f'in between [t_o, t_n] = [{t_o}, {t_n}]'
                logger.info(f'[idx: {loop_idx}] |~> [located event] {msg1} {msg2}')
                for idx in event_ids[-1]:
                    if terminating[idx] > 0.0:
                        logger.info(f'[idx: {loop_idx}] |~> [terminal event] stop numerical integration!')
                        break
                else: continue
                break
            event_func_eval_o = event_func_eval_n

        h_new: float = h_n
        if step_control:
            ''' err control '''
            err = np.linalg.norm(x_full - x_n)
            if err != 0.0:
                h_cand: float = h_damp*(err_tol*(h_n**2.0))/err
                if (err > err_tol) or (h_cand > 1.5*h_n): h_new = h_cand

        if step_control and (abs(h_new - h_n) > stepper_inst.eps):  # not report.success:
            stepper_inst.reject(h_next = h_new)
            if h_new > h_n: logger.info(f'[idx: {loop_idx}] |====> increase h from {h_n} to {stepper_inst.h_next}')
            else: logger.info(f'[idx: {loop_idx}] |--> decrease h from {h_n} to {stepper_inst.h_next}')
        else:
            stepper_inst.accept()
            xs.append(x_n)

    return EulerResult(ts = ts, xs = xs, hs = hs,
                       events = events, event_ids = event_ids,
                       opts = opts, event_opts = event_opts)


def _has_events(t_a, event_func_eval_a, t_b, event_func_eval_b, orientation) -> tuple[bool, float, list[int]]:
    hs_event: list[float] = list()
    event_ids_n: list[int] = list()

    ''' locate potential event(s) between t_a and t_b '''
    for event_id, (e_a_i, e_b_i, ori_i) in enumerate(zip(event_func_eval_a, event_func_eval_b, orientation)):
        if (e_a_i <= 0.0) and (e_b_i >= 0.0) and (ori_i >= 0):  # change "-" -> "+"
            hs_event.append(secant_method_step(t_a, t_b, e_a_i, e_b_i) - t_a)
            event_ids_n.append(event_id)
        elif (e_a_i >= 0.0) and (e_b_i <= 0.0) and (ori_i <= 0):  # change "+" -> "-"
            hs_event.append(secant_method_step(t_a, t_b, e_a_i, e_b_i) - t_a)
            event_ids_n.append(event_id)

    ''' identify next event among all potential events '''
    if len(hs_event) > 0:
        sorted_map = [idx for idx, _ in sorted(enumerate(hs_event), key = lambda arg: arg[1])]
        hs_event = [hs_event[idx] for idx in sorted_map]  # sort to find closest event in the future
        event_ids_n = [event_ids_n[idx] for idx in sorted_map]
        h_event: float | None = None
        for entry in hs_event:
            if entry > 0.0:
                h_event = entry
                break
        if not (h_event is None):
            hs_tmp = [h_entry - h_event for h_entry in hs_event]  # find multiple events taking place at once
            event_ids_n = [entry for idx, entry in enumerate(event_ids_n) if abs(hs_tmp[idx]) < 1.0e-11]  # filter ids
            return True, h_event, event_ids_n
    return False, -1.0, list()


def _detect_events(t_o, x_o, t_a, x_a, t_b, x_b, euler_step,
                   orientation, event_func, event_func_eval_a = None, event_func_eval_b = None,
                   depth: int = -1) -> tuple[bool, np.ndarray, float, np.ndarray, list[int]]:
    depth += 1
    if event_func_eval_a is None: event_func_eval_a = event_func(x_a, t_a)
    if event_func_eval_b is None: event_func_eval_b = event_func(x_b, t_b)

    does_have, h_cand, event_ids_n = _has_events(t_a, event_func_eval_a, t_b, event_func_eval_b, orientation)
    if not does_have: return False, x_o, h_cand, event_func_eval_a, event_ids_n

    ''' check if h_cand good enough ... '''
    t_cand = t_a + h_cand
    report = euler_step(x_o, t_o, t_cand, (t_cand - t_o))
    x_cand = report.x
    event_func_eval_cand = event_func(x_cand, t_cand)
    if min(t_b - t_cand, t_cand - t_a) < 1.0e-8: return True, x_cand, h_cand, event_func_eval_cand, event_ids_n

    ''' ... otherwise check and refine on the left [t_a, t_cand] ... '''
    out_l = _detect_events(t_o, x_o, t_a, x_a, t_cand, x_cand, euler_step,
                           orientation, event_func, event_func_eval_a, event_func_eval_cand, depth)
    does_have_l, x_cand_l, h_cand_l, event_func_eval_cand_l, event_ids_n_l = out_l
    if does_have_l: return True, x_cand_l, h_cand_l, event_func_eval_cand_l, event_ids_n_l
    ''' ... and if still necessary also check and refine on the right [t_cand, t_b] '''
    out_r = _detect_events(t_o, x_o, t_cand, x_cand, t_b, x_b, euler_step,
                           orientation, event_func, event_func_eval_cand, event_func_eval_b, depth)
    does_have_r, x_cand_r, h_cand_r, event_func_eval_cand_r, event_ids_n_r = out_r
    if does_have_r: return True, x_cand_r, h_cand + h_cand_r, event_func_eval_cand_r, event_ids_n_r

    return True, x_cand, h_cand, event_func_eval_cand, event_ids_n  # this line should not/hardly be reachable
    # raise AssertionError('XXX')  # "strict rule" instead of line above!


def generate_euler_step(nl_solver: NLSolver,
                        sys_func: SysFunc, d_func: LeadingDerivativeTerm, sys_jac: SysJac | None,
                        rtol: float, atol: float, logger: logging.Logger):
    def euler_step(x_o: np.ndarray, t_o: float, t_n: float, h_n: float):
        h_func = create_h_func(x_o, t_o, t_n, h_n, sys_func, d_func)
        if sys_jac is None: h_jac = generate_jac_fd(h_func, 1.0e-8)
        else: h_jac = create_h_jac(x_o, t_o, t_n, h_n, sys_jac, d_func)

        report: NLReport = nl_solver(func = h_func, x0 = x_o, jac = h_jac)

        return report
    return euler_step


def secant_method_step(t_o: float, t_n: float, x_o: float, x_n: float) -> float:  # <tomarkdown:IGNORE>
    return t_n - ((t_n - t_o)/(x_n - x_o))*x_n


def _finite_diff_d_func(x_n, t_n, x_o, t_o, h_n, d_func):  # <tomarkdown:IGNORE>
    return (d_func(x_n, t_n) - d_func(x_o, t_o))/h_n


def create_h_func(x_o: np.ndarray, t_o: float, t_n: float, h_n: float, sys_func, d_func):
    '''
    Generates the $H(x)$ function representing an implicit Euler step, where

    $$
    H({\bf x}) \equiv f\left( \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n}, {\bf x}, t_{n+1} \right)
    $$

    **args**

        - x_o: or $x_n$ - the 'old'  aka  preceding  aka  the latest known approximation to $x(t)$
        - t_o: or $t_n$ - the 'old' time point corresponding to x_o
        - t_n: or $t_{n+1} \equiv t_n + h_n$ - the 'new' time point corresponding to (the yet unknown) x_n
        - h_n: or $h_n \equiv t_{n+1} - t_n$ the current time step width
        - sys_func: the dae outer system function
        - d_func: the (dae inner) leading derivative term expression
    '''
    def h_func(x_n: np.ndarray):  # <tomarkdown:IGNORE>
        return sys_func(_finite_diff_d_func(x_n, t_n, x_o, t_o, h_n, d_func), x_n, t_n)
    return h_func


def create_h_jac(x_o: np.ndarray, t_o: float, t_n: float, h_n: float, sys_jac, d_func):
    '''
    Generates the function computing the Jacobian $J_H[{\mathbf x}]$ of $H(x)$, where

    $$
    H({\bf x}) \equiv f\left( \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n}, {\bf x}, t_{n+1} \right)
    $$

    via the user supplied "sys_jac" function

    $$
    \mathrm{sys\_jac}(\alpha, y, x, s) \equiv \alpha\cdot
        \left[\partial_y f(y, x, s)\right] \cdot \left[\partial_x d(x, s)\right] +
        \left[\partial_x f(y, x, s)\right]
    $$

    as follows

    $$
    \mathrm{sys\_jac}\left(\tfrac 1h, \tfrac{d({\bf x}, t_{n+1}) - d(x_n, t_n)}{t_{n+1} - t_n},
        {\mathbf x}, t_{n+1}\right) = J_H[{\mathbf x}]
    $$

    **args**

        - x_o: or $x_n$ - the 'old'  aka  preceding  aka  the latest known approximation to $x(t)$
        - t_o: or $t_n$ - the 'old' time point corresponding to x_o
        - t_n: or $t_{n+1} \equiv t_n + h_n$ - the 'new' time point corresponding to (the yet unknown) x_n
        - h_n: or $h_n \equiv t_{n+1} - t_n$ the current time step width
        - sys_jac: the optionally user-provided system Jacobian
        - d_func: the (dae inner) leading derivative term expression

    **returns**

        - the function computing the Jacobian
    '''
    def h_jac(x_n: np.ndarray):  # <tomarkdown:IGNORE>
        return sys_jac(1.0 / h_n, _finite_diff_d_func(x_n, t_n, x_o, t_o, h_n, d_func), x_n, t_n)
    return h_jac
