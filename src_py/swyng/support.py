from typing import Any

import logging


class OptionsTemplate(object):  # <tomarkdown:IGNORE>

    def __getitem__(self, item: str): return self.__getattribute__(item)

    def __setitem__(self, name: str, val: Any): return self.__setattr__(name, val)

    def get(self, key, default = None):
        try: return self[key]
        except: pass
        return default


def prep_logger(default_name: str,
                logger: logging.Logger | bool | str = True) -> logging.Logger:
    if isinstance(logger, logging.Logger): return logger
    if isinstance(logger, str): return logging.getLogger(logger)
    if isinstance(logger, bool):
        logger_flag: bool = logger
        logger: logging.Logger = logging.getLogger(default_name)
        logger.handlers.clear()
        logger.setLevel(logging.DEBUG)
        if not (logger_flag is False):
            c_handler = logging.StreamHandler()
            c_handler.setLevel(logging.INFO)
            c_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
            c_handler.setFormatter(c_format)
            logger.addHandler(c_handler)
        else:
            null_handler = logging.NullHandler()
            null_handler.setLevel(logging.INFO)
            logger.addHandler(null_handler)
        return logger
    raise AssertionError('which logger?!')
