import numpy as np

import scipy.sparse as sps

from swyng.support import prep_logger, OptionsTemplate

import logging

from typing import Protocol, Callable, Any, Optional, TypeAlias


class _BaseLNLOpts(OptionsTemplate):
    atol: float = 1.0e-8
    rtol: float = 1.0e-3

    def __init__(self, 
                 atol: float = 1.0e-8, 
                 rtol: float = 1.0e-5):
        self.atol = atol
        self.rtol = rtol


class LinOpts(_BaseLNLOpts): pass


class NLOpts(_BaseLNLOpts): pass


class _BaseLNLReport(OptionsTemplate):  # LNL - lin/non-lin
    x: np.ndarray | None = None  # None in case of no convergence
    success: bool = None
    msg: str = None  # optional

    def __init__(self,
                 x: Optional[np.ndarray] = None,
                 success: Optional[bool] = None,
                 msg: Optional[str] = ''):
        self.x = x
        if success is None: self.success = False
        else: self.success = success
        self.msg = msg


class LinReport(_BaseLNLReport): pass


class NLReport(_BaseLNLReport): pass


class NLSysFunc(Protocol):
    def __call__(self, x: np.ndarray) -> np.ndarray: ...


NLSysJacMat: TypeAlias = np.ndarray | sps.base.spmatrix


class NLSysJac(Protocol):
    def __call__(self, x: np.ndarray) -> NLSysJacMat: ...


class LinSolver(Protocol):
    '''
    finds x* solving: A @ x* = b
    '''

    def __call__(self,
                 A: NLSysJacMat,
                 b: np.ndarray,
                 x0: np.ndarray) -> LinReport: ...


class NLSolver(Protocol):
    '''
    finds x* solving: 0 = func(x*)
    '''

    def __call__(self,
                 func: NLSysFunc,
                 jac: NLSysJac,
                 x0: np.ndarray) -> NLReport: ...
