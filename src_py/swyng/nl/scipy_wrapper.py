import numpy as np

from scipy.optimize import root
from scipy.linalg import solve
from scipy.sparse.linalg import spsolve
from scipy.sparse import issparse

from swyng.support import logging, prep_logger
from swyng.nl.support import (LinOpts, LinReport, LinSolver,
                              NLOpts, NLReport, NLSolver,
                              NLSysFunc, NLSysJac, NLSysJacMat)

from typing import Callable, Any, Optional


class SPSolveWrapper(object):
    logger: logging.Logger = None
    logger_default_name: str = 'swyng.nl.SPSolveWrapper'
    opts: LinOpts = None

    def __init__(self,
                 logger: logging.Logger | bool | str = False,
                 opts: LinOpts | dict[str, Any] | None = None):
        self.logger = prep_logger(self.__class__.logger_default_name, logger)
        if opts is None: self.opts = LinOpts()
        elif isinstance(opts, dict): self.opts = LinOpts(**opts)
        else: self.opts = opts

    def __call__(self,
                 A: NLSysJacMat,
                 b: np.ndarray,
                 x0: np.ndarray) -> LinReport:
        atol: float = self.opts.atol
        rtol: float = self.opts.rtol

        try:
            if issparse(A): x_cand: np.ndarray = spsolve(A = A, b = b)
            else: x_cand: np.ndarray = solve(a = A, b = b)
        except Exception: return LinReport(x = x0, success = False, msg = "inner lin-solver failed!")

        success: bool = False
        msg: str = 'inner lin-solver did not converge!'
        if np.linalg.norm(A@x_cand - b) <= atol + rtol*np.linalg.norm(A@x0 - b):
            success = True
            msg = 'success'
        return LinReport(x = x_cand, success = success, msg = msg)


class RootWrapper(object):
    logger: logging.Logger = None
    logger_default_name: str = 'swyng.nl.RootWrapper'
    opts: NLOpts = None

    def __init__(self,
                 logger: logging.Logger | bool | str = False,
                 opts: NLOpts | dict[str, Any] | None = None):
        self.logger = prep_logger(self.__class__.logger_default_name, logger)
        if opts is None: self.opts = NLOpts()
        elif isinstance(opts, dict): self.opts = NLOpts(**opts)
        else: self.opts = opts

    def __call__(self,
                 func: NLSysFunc,
                 jac: NLSysJac,
                 x0: np.ndarray) -> NLReport:
        ''' TODO: doc '''
        atol = self.opts.atol
        rtol = self.opts.rtol

        report_root = root(fun = func,
                           x0 = x0,
                           jac = jac,
                           tol = None,
                           options = dict(ftol = rtol,
                                          xtol = self.opts.get('xtol', rtol/10.0)))

        success: bool = False
        x: np.ndarray = report_root.x
        if np.linalg.norm(func(x)) <= atol + rtol*np.linalg.norm(func(x0)): success = True

        re = NLReport(x = x, success = success)
        re.orig_report = report_root
        return re
