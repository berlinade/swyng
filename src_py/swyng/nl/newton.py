import numpy as np

from scipy.optimize import root

from swyng.support import logging, prep_logger
from swyng.nl.support import NLOpts, NLReport, LinReport, LinSolver, NLSolver, NLSysFunc, NLSysJac, NLSysJacMat

from swyng.nl.scipy_wrapper import SPSolveWrapper

from typing import Callable, Any, Optional


class NewtonOpts(NLOpts):
    max_iter: int = None  # 50
    refresh_jac: int = None  # 1
    broyden_update: int = None  # 0
    alpha_min: float = None  # 1.0e-14

    def __init__(self,
                 atol: float = 1.0e-8,
                 rtol: float = 1.0e-5,
                 max_iter: int = 50,
                 refresh_jac: int = 1,
                 broyden_update: int = 0,
                 alpha_min: float = 1.0e-14):
        super().__init__(atol = atol, rtol = rtol)
        self.max_iter = max_iter
        self.refresh_jac = refresh_jac
        self.broyden_update = broyden_update
        self.alpha_min = alpha_min


class Newton(object):
    logger: logging.Logger = None
    logger_default_name: str = 'swyng.nl.Newton'
    opts: NewtonOpts = None
    lin_solve: LinSolver = None

    def __init__(self,
                 lin_solve: LinSolver | None = None,
                 logger: logging.Logger | bool | str = False,
                 opts: NewtonOpts | dict[str, Any] | None = None):
        self.logger = prep_logger(self.__class__.logger_default_name, logger)
        if opts is None: self.opts = NewtonOpts()
        elif isinstance(opts, dict): self.opts = NewtonOpts(**opts)
        else: self.opts = opts
        self.lin_solve = lin_solve if (not (lin_solve is None)) else SPSolveWrapper(logger = False,
                                                                                    opts = dict(atol = self.opts.atol,
                                                                                                rtol = self.opts.rtol,))

    def __call__(self,
                 func: NLSysFunc,
                 jac: NLSysJac,
                 x0: np.ndarray) -> NLReport:
        ''' TODO: doc '''
        atol = self.opts.atol
        rtol = self.opts.rtol
        max_iter: int = self.opts['max_iter']
        refresh_jac: int = self.opts['refresh_jac']
        broyden_update: int = self.opts['broyden_update']
        alpha_min: float = self.opts['alpha_min']
        lin_solve = self.lin_solve
        logger = self.logger

        if max_iter <= 0: raise AssertionError('max_iter > 0!')
        if refresh_jac < 1: refresh_jac = max_iter + 1
        if broyden_update < 1: broyden_update = max_iter + 1
        if alpha_min <= 0.0: raise AssertionError('alpha_min > 0.0!')

        x0_norm: float = np.linalg.norm(x0)
        f0: np.ndarray = func(x0)
        f0_norm: float = np.linalg.norm(f0)

        loop_idx: int = 0
        success: bool = False
        msg: str = ''
        msg_info: str | None = None
        msg_err: str | None = None
        do_refresh_jac: bool = True
        do_broyden_update: bool = False
        x_o: np.ndarray = x0
        x_o_norm: float = x0_norm
        f_o: np.ndarray = f0
        f_o_norm: float = f0_norm
        alpha_o: float = 1.0
        jac_mat: np.ndarray = np.array([])  # init empty for type annotation
        while True:
            if do_refresh_jac:
                jac_mat: NLSysJacMat = jac(x_o)  # Jac_approx_for_debug(func, x_o)  #
                logger.info(f'[idx: {loop_idx}] refresh Jacobian')
            elif do_broyden_update: ...  # TODO

            ''' compute Newton direction '''
            lin_rep: LinReport = lin_solve(jac_mat, -f_o, x_o)
            if lin_rep.success: dir_newt: np.ndarray = lin_rep.x  # Newton direction
            elif not do_refresh_jac:
                do_refresh_jac = True
                do_broyden_update = False
                logger.warning(f'[idx: {loop_idx}] computation of Newton direction failed -> will refresh Jacobian!')
                continue  # redo current cycle
            else:
                logger.error(f'[idx: {loop_idx}] could not compute Newton direction!')
                return NLReport(x = x0, success = False, msg = "could not compute Newton direction!")

            alpha_n: float = 4.032*alpha_o
            while alpha_n > alpha_min:
                x_n: np.ndarray = x_o + alpha_n*dir_newt
                try:
                    f_n: np.ndarray = func(x_n)
                    f_n_norm: float = np.linalg.norm(f_n)
                    if f_n_norm <= f_o_norm:  # f_n_norm < 0.95*f_o_norm:
                        x_n_norm: float = np.linalg.norm(x_n)
                        break
                except Exception: pass
                alpha_n /= 2.016  # == 4.032/2.0, such that alpha_n = 1.0 (exactly) exists after 2 shrinkings!
            else:
                if not do_refresh_jac:
                    do_refresh_jac = True
                    do_broyden_update = False
                    logger.warning(f'[idx: {loop_idx}] alpha < alpha_min -> will refresh Jacobian!')
                    continue  # redo current cycle
                msg: str = f'[idx: {loop_idx}] dampening coeff ("alpha") drop below alpha_min = {alpha_min}!'
                logger.error(msg)
                return NLReport(x = x0, success = False, msg = msg)

            ''' breaking conditions - part I '''
            c_i: bool = f_n_norm <= atol + rtol*f_o_norm
            c_ii: bool = np.linalg.norm(f_n - f_o) <= atol + rtol*f_o_norm
            c_iii: bool = np.linalg.norm(x_n - x_o) <= atol + rtol*x_o_norm
            if c_i or c_ii or c_iii:
                success: bool = True
                msg_info = f'[idx: {loop_idx}] success'
                break

            ''' prepare next cycle '''
            loop_idx += 1

            x_o = x_n
            x_o_norm = x_n_norm
            f_o = f_n
            f_o_norm = f_n_norm
            alpha_o = alpha_n

            do_refresh_jac = False  # reset
            do_broyden_update = False  # reset
            if loop_idx % refresh_jac == 0: do_refresh_jac = True
            elif loop_idx % broyden_update == 0: do_broyden_update = True

            ''' breaking conditions - part II '''
            if loop_idx == max_iter:
                success: bool = False
                msg_err = f'[idx: {loop_idx}] no convergence; max_iter exhausted!'
                break

        if msg_info: logger.info(msg_info)
        if msg_err: logger.info(msg_err)
        return NLReport(x = x_o, success = success, msg = msg)


# class DebugNewton(object):
#     logger: logging.Logger = None
#     logger_default_name: str = 'swyng.nl.DNewton'
#     opts: NewtonOpts = None
#     lin_solve: LinSolver = None
#
#     def __init__(self,
#                  lin_solve: LinSolver | None = None,
#                  logger: logging.Logger | bool | str = False,
#                  opts: NewtonOpts | dict[str, Any] | None = None):
#         self.logger = prep_logger(self.__class__.logger_default_name, logger)
#         if opts is None: self.opts = NewtonOpts()
#         elif isinstance(opts, dict): self.opts = NewtonOpts(**opts)
#         else: self.opts = opts
#         self.lin_solve = lin_solve if (not (lin_solve is None)) else SPSolveWrapper(logger = False,
#                                                                                     opts = dict(atol = self.opts.atol,
#                                                                                                 rtol = self.opts.rtol,))
#
#     @staticmethod
#     def make_hh(A, b):
#         def hh(x): return A@x - b
#         return hh
#
#     @staticmethod
#     def Jac_approx_for_debug(f, x, dx = 1e-8):
#         n = len(x)
#         func = f(x)
#         jac = np.zeros((n, n))
#         for j in range(n):  # through columns to allow for vector addition
#             Dxj = (abs(x[j])*dx if x[j] != 0 else dx)
#             x_plus = [(xi if k != j else xi + Dxj) for k, xi in enumerate(x)]
#             jac[:, j] = (f(np.array(x_plus)) - func)/Dxj
#         return jac
#
#     def __call__(self,
#                  func: NLSysFunc,
#                  jac: NLSysJac,
#                  x0: np.ndarray) -> NLReport:
#         ''' TODO: doc '''
#         atol = self.opts.atol
#         rtol = self.opts.rtol
#         max_iter: int = self.opts['max_iter']
#         refresh_jac: int = self.opts['refresh_jac']
#         broyden_update: int = self.opts['broyden_update']
#         alpha_min: float = self.opts['alpha_min']
#         lin_solve = self.lin_solve
#         logger = self.logger
#
#         if max_iter <= 0: raise AssertionError('max_iter > 0!')
#         if refresh_jac < 1: refresh_jac = max_iter + 1
#         if broyden_update < 1: broyden_update = max_iter + 1
#         if alpha_min <= 0.0: raise AssertionError('alpha_min > 0.0!')
#
#         x_curr = x0 + 0.001
#
#         for _idx in range(10):
#             # J = jac(x_curr)
#             # print(J)
#             J = self.Jac_approx_for_debug(func, x_curr)
#             fun = func(x_curr)
#             rep = root(self.make_hh(J, -fun), x_curr)  # lin_solve(J, -fun, x_curr)
#             x_curr = x_curr + rep.x
#
#         return NLReport(x = x_curr, success = True, msg = '')
