import numpy as np

from swyng.support import logging, prep_logger
from swyng.nl.support import NLOpts, NLSolver, NLReport, NLSysFunc, NLSysJac

from swyng.nl.newton import Newton

from typing import Callable, Any, Optional, Union


def generate_simple_homotopy_strat(lam: float,
                                   func: NLSysFunc,
                                   jac: NLSysJac,
                                   x0: np.ndarray,
                                   x0_hom: Optional[np.ndarray] = None,
                                   f0: Union[np.ndarray, None] = None) -> tuple[np.ndarray,
                                                                                NLSysFunc,
                                                                                NLSysJac]:
    if f0 is None: f0: np.ndarray = func(x0)

    def simple_homotopy_strat(x: np.ndarray) -> np.ndarray: return func(x) - (1.0 - lam)*f0

    return f0, simple_homotopy_strat, jac


class Homotopy(object):
    logger: logging.Logger = None
    logger_default_name: str = 'swyng.nl.Homotopy'
    opts: NLOpts = None
    nl_solver: NLSolver = None
    hom_fun_gen = None

    def __init__(self,
                 nl_solver: NLSolver | None = None,
                 homotopy_fun_generator = None,
                 logger: logging.Logger | bool | str = False,
                 opts: NLOpts | dict[str, Any] | None = None):
        self.logger = prep_logger(self.__class__.logger_default_name, logger)
        if opts is None: self.opts = NLOpts()
        elif isinstance(opts, dict): self.opts = NLOpts(**opts)
        else: self.opts = opts
        self.nl_solver = nl_solver if (not (nl_solver is None)) else Newton(logger = False,
                                                                            opts = dict(atol = self.opts.atol,
                                                                                        rtol = self.opts.rtol,))
        hom_fun_gen = homotopy_fun_generator
        self.hom_fun_gen = hom_fun_gen if (not (hom_fun_gen is None)) else generate_simple_homotopy_strat

    def __call__(self,
                 func: NLSysFunc,
                 jac: NLSysJac,
                 x0: np.ndarray) -> NLReport:
        ''' TODO: doc '''
        nl_solver = self.nl_solver
        hom_fun_gen = self.hom_fun_gen
        logger = self.logger

        f0: Union[np.ndarray, None] = None
        x0_hom: np.ndarray = x0
        lam_base: float = 0.0
        lam_h: float = 1.0
        fails_in_a_row: int = 0
        succs_in_a_row: int = 0
        overall_success: bool = False
        while True:
            f0, func_hom, jac_hom = hom_fun_gen(lam = lam_base + lam_h,
                                                func = func, jac = jac,
                                                x0 = x0, x0_hom = x0_hom,
                                                f0 = f0)
            report_nl = nl_solver(func = func_hom, jac = jac_hom, x0 = x0_hom)

            if report_nl.success:
                lam_base += lam_h
                x0_hom = report_nl.x
                msg_logger: str = f'homotopy success for lambda = {lam_base}'
                if abs(lam_base - 1.0) < 1.0e-11:  # i.e. lam_base == 1.0 but being faithful to round-off error
                    if fails_in_a_row + succs_in_a_row > 0: logger.info(msg_logger)
                    overall_success = True  # winning condition
                    break
                elif lam_base > 1.0:
                    err_msg: str = f"lam_base: {lam_base} too big!"
                    logger.error(err_msg)
                    raise AssertionError(err_msg)

                fails_in_a_row = 0
                succs_in_a_row += 1

                if succs_in_a_row > 2:
                    msg_logger += f' | expand lam_h from {lam_h}'
                    lam_h *= 1.989  # ~ 2.0 but noised to avoid odd symmetries
                    logger.info(msg_logger + f' to {lam_h}')
                else: logger.info(msg_logger)
                lam_h = min(lam_h, 1.0 - lam_base)  # clipping lam_h
            else:
                succs_in_a_row = 0
                fails_in_a_row += 1

                if fails_in_a_row >= 20: break  # no convergence can be enforced along homotopy path

                msg_logger: str = f'homotopy fails at lambda = {lam_base + lam_h} | shrink lam_h from {lam_h}'
                lam_h *= 0.489  # ~ 0.5 but noised to avoid odd symmetries
                logger.info(msg_logger + f' to {lam_h} | hence new lambda = {lam_base + lam_h}')

        if overall_success: x = x0_hom
        else: x = x0
        re = NLReport(x = x, success = overall_success)
        re.latest_report = report_nl
        return re
